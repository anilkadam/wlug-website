<!DOCTYPE html>
<html>
<head>
    <!--link href='member/css/bootstrap.css' rel='stylesheet' /-->
    <link href='member/css/rotating-card.css' rel='stylesheet' />
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name1='viewport' />

    <!--link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"-->

    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
    <?php include('header.php');?>

</head>
<body>         <strong><h2 class="text-center">  Main Board</h2></strong> 
<div class="body" >  
<div class="container">
    <div class="row">
        
        <div class="col-sm-10 col-sm-offset-1">
         <div class="col-md-4 col-sm-6" style="margin-top: 60px">
             <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="member/images/rotating_card_thumb2.png"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="member/images/anil.jpg"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name1">Anil Kadam</h3>
                                <p class="profession">Developer</p>
                                <p class="text-center">"Hii friends i'm wlugian Anil,<br> I am a Developer"</p>
                            </div>
                            
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">CUMMINITY | KNOWLEDGE | SHARE<h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Area of interest</h4>
                                <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many others...</p>
                                <br>
                                <br>
                                
                                 <h4 class="text-center">****Contact****</h4>
                                <p class="text-center"><font size="4"><i class="fa fa-mobile" aria-hidden="true"> 9970053448</i></font></p>
                                <p class="text-center"><font size="4"><i class="fa fa-envelope" aria-hidden="true"> anilkadam210@gmail.com</i></font></p>
                                

                            </div>
                        </div>
                        <div class="footer">
                            <div class="social-links text-center">
                                <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->
<!--         <div class="col-sm-1"></div> -->
        <div class="col-md-4 col-sm-6" style="margin-top: 60px">
             <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="member/images/rotating_card_thumb2.png"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="member/images/anil.jpg"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name1">Anil Kadam</h3>
                                <p class="profession">Developer</p>
                                <p class="text-center">"Hii friends i'm wlugian Anil,<br> I am a Developer"</p>
                            </div>
                            
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">CUMMINITY | KNOWLEDGE | SHARE<h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Area of interest</h4>
                                <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many others...</p>
                                <br>
                                <br>
                                
                                 <h4 class="text-center">****Contact****</h4>
                                <p class="text-center"><font size="4"><i class="fa fa-mobile" aria-hidden="true"> 9970053448</i></font></p>
                                <p class="text-center"><font size="4"><i class="fa fa-envelope" aria-hidden="true">anilkadam210@gmail.com</i></font></p>
                                

                            </div>
                        </div>
                        <div class="footer">
                            <div class="social-links text-center">
                                <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> <!-- end col sm 3 -->

        <div class="col-md-4 col-sm-6" style="margin-top: 60px">
             <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="member/images/rotating_card_thumb2.png"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="member/images/anil.jpg"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name1">Anil Kadam</h3>
                                <p class="profession">Developer</p>
                                <p class="text-center">"Hii friends i'm wlugian Anil,<br> I am a Developer"</p>
                            </div>
                            
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">CUMMINITY | KNOWLEDGE | SHARE<h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Area of interest</h4>
                                <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many others...</p>
                                <br>
                                <br>
                                
                                 <h4 class="text-center">****Contact****</h4>
                                <p class="text-center"><font size="4"><i class="fa fa-mobile" aria-hidden="true"> 9970053448</i></font></p>
                                <p class="text-center"><font size="4"><i class="fa fa-envelope" aria-hidden="true">anilkadam210@gmail.com</i></font></p>
                                

                            </div>
                        </div>
                        <div class="footer">
                            <div class="social-links text-center">
                                <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> 

        <div class="col-md-4 col-sm-6" style="margin-top: 60px">
             <div class="card-container">
                <div class="card">
                    <div class="front">
                        <div class="cover">
                            <img src="member/images/rotating_card_thumb2.png"/>
                        </div>
                        <div class="user">
                            <img class="img-circle" src="member/images/male.jpe"/>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h3 class="name1">Anil Kadam</h3>
                                <p class="profession">Developer</p>
                                <p class="text-center">"Hii friends i'm wlugian Anil,<br> I am a Developer"</p>
                            </div>
                            
                        </div>
                    </div> <!-- end front panel -->
                    <div class="back">
                        <div class="header">
                            <h5 class="motto">CUMMINITY | KNOWLEDGE | SHARE<h5>
                        </div>
                        <div class="content">
                            <div class="main">
                                <h4 class="text-center">Area of interest</h4>
                                <p class="text-center">Web design, Adobe Photoshop, HTML5, CSS3, Corel and many others...</p>
                                <br>
                                <br>
                                
                                 <h4 class="text-center">****Contact****</h4>
                                <p class="text-center"><font size="4"><i class="fa fa-mobile" aria-hidden="true"> 9970053448</i></font></p>
                                <p class="text-center"><font size="4"><i class="fa fa-envelope" aria-hidden="true">anilkadam210@gmail.com</i></font></p>
                                

                            </div>
                        </div>
                        <div class="footer">
                            <div class="social-links text-center">
                                <a href="#" class="facebook"><i class="fa fa-facebook fa-fw"></i></a>
                                <a href="#" class="google"><i class="fa fa-google-plus fa-fw"></i></a>
                                <a href="#" class="twitter"><i class="fa fa-twitter fa-fw"></i></a>
                            </div>
                        </div>
                    </div> <!-- end back panel -->
                </div> <!-- end card -->
            </div> <!-- end card-container -->
        </div> 

        </div> <!-- end col-sm-10 -->
    </div> <!-- end row -->
    
</div>
</div>
 


<script src="member/js/bootstrap.js" type="text/javascript"></script>
<script src="member/js/bootstrap.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $().ready(function(){
        $('[rel="tooltip"]').tooltip();

    });

    function rotateCard(btn){
        var $card = $(btn).closest('.card-container');
        console.log($card);
        if($card.hasClass('hover')){
            $card.removeClass('hover');
        } else {
            $card.addClass('hover');
        }
    }
</script>
<?php include('footer.php');?>

</body>
</html>
