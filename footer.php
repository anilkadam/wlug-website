 <section id="footer-menu" class="animated sections footer-menu">
        <center>
            <div class="container">
                <div class="row">
                    <div class="footer-menu-wrapper">
                        <div class="col-md-12 col-sm-19 col-xs-20">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Quick Links</h5>
                                    <ul>
                                        <li>Virtual Library</li>
                                        <li>OpenSource Technology </li>
                                        <li>WLUG Members</li>
                                        <li>About Us</li>
                                        <li>Recent Updates</li>
                                    </ul>
                                </div>
                            </div>
                           <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Legal</h5>
                                    <ul>
                                        <li>PRIVACY & COOKIES</li>
                                        <li>TERMS OF USE</li>
                                        <li>TRADEMARKS</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Information</h5>
                                    <ul>
                                        <li>SUPPORT</li>
                                        <li>DEVELOPERS</li>
                                        <li>BLOG</li>
                                        <li>Alumni Portal</li>
                                        <li>OpenSource Technology</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </section>


    <!--Footer-->
    <footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-wrapper">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="footer-brand">
                            <img src="assets/images/wluglogo.png" alt="logo" height="70" width="60"  />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p> <i class="fa fa-copyright"></i> by <a target="_blank" href="http://wcewlug.org"> WLUG </a>2017. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>



        <div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>

        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
    </body>
</html>
