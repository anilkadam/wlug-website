<!DOCTYPE html>
<html>
<head>
	<title>Club Services</title>
	<meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>WLUG</title>
        <link rel="shortcut icon" href="assets/images/wluglogo.png" type="image/x-icon">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <link rel="stylesheet" type="text/css" href="assets/css/memberDropDownMenu.css">

        <!--Animate css file-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
        <!--Google fonts links-->
        <link href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">


        <!--For Plugins external css-->
        <link rel="stylesheet" href="assets/css/plugins.css" />
        <link rel="stylesheet" href="assets/css/roboto-webfont.css" />

        <!--Theme custom css -->
        <link rel="stylesheet" href="assets/css/style.css">

        <!--Theme Responsive css-->
        <link rel="stylesheet" href="assets/css/responsive.css" />

        <!--Clubservice style-->
        <link rel="stylesheet" href="assets/css/clubservice.css">
       	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700" rel="stylesheet" />

        <script src="assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>

	<style>
    .container1
        {
            min-width:400%;
        }
    </style>
    <!-- Latest compiled and minified CSS -->
</head>
<body>
<section id="social" class="social">
            <div class="container">
                <!-- Example row of columns -->
                <div class="row">
                    <div class="social-wrapper">
                        <div class="col-md-6">
                            <div class="social-icon">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-google-plus"></i></a>
                                <a href="#"><i class="fa fa-linkedin"></i></a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="social-contact">
                                <a href="#"><i class="fa fa-phone"></i>9021526132</a>
                                <a href="#"><i class="fa fa-envelope"></i>rohitkhot1997@gmail.com</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div> <!-- /container -->       
        </section>

        <nav class="navbar navbar-default">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                
                <div class="navbar-header">
                    <a class="navbar-nav" href="#"><img src="assets/images/wluglogo.png" alt="Logo" height="60" width="90" style="margin-left: 10px" /></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <ul class="nav navbar-nav navbar-right">
                        <li class="active"><a href="#home">Home</a></li>
                        <li><a href="#event">Events</a></li>
                        <li><a href="#service">Club Service</a></li>
                        <li><div class="dropdown">
                            <a href="#" class="dropbtn fontcolor">Members</a>
                            <div class="dropdown-content">
                                <a href="member.html">Main Board</a>
                                <a href="#">Mentor Board</a>
                                <a href="#">Asst. Board</a>
                                <a href="#">Member</a>
                            </div>    
                    </div></li>
                        <li><a href="#business">Gallery</a></li>
                        <li><a href="#contact">Alumni Portal</a></li>
                        <li><a href="#contact">Contact</a></li>
                        <li class="login"><a href="editorLogin.php">Editor</a></li>
                    <!--li class="login"><button type="button" class="btn btn-success">Editor Sign in</button></li-->
                    </ul>

                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>

    <?php
//include('header.php');
require_once('connectcs.php');

error_reporting(0);

mysql_select_db($connectcs,$dbname);
$qur1="select * from  clubservice";
$res1=mysqli_query($connectcs,$qur1);
$num=mysqli_num_rows($res1);

?>
    
    
    
    
    <section>
   <div class="wrapper1">
  <h1>CLUB SERVICE</h1>
<div class="container">
    
       
       <?php
$val=0;	
$flag=0;
while($row=mysqli_fetch_array($res1))
{
	$flag=1;
$val++;	?>


<div class="col-sm-4">       
  <div class="cols">
			<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(assets/images/cs.jpg)">
						<div class="inner">
							<p><strong><font color="red" size="5"><?php echo $row['csname'];?></font></strong></p>
              <span><?php echo "Club Service : ".$val;?></span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
						  <!--p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p-->
						  <button type="button" class="btn btn-primary">Download</button>
						</div>
					</div>
				</div>
			</div>
                      

       </div>
    
       </div>
       <?php    }
            ?>   
       </div>
        </div>
    </section>              
       
		<!--	<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/501/501/)">
						<div class="inner">
							<p>Command And Editor</p>
              <span>Club service 2</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>
		<!--	<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/502/502/)">
						<div class="inner">
							<p>Hacking</p>
              <span>Club service 3</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>-->
			<!--div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/503/503/)">
						<div class="inner">
							<p>Clossyo</p>
              <span>Lorem ipsum</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/504/504/">
						<div class="inner">
							<p>Rendann</p>
              <span>Lorem ipsum</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/505/505/)">
						<div class="inner">
							<p>Reflupper</p>
              <span>Lorem ipsum</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/506/506/)">
						<div class="inner">
							<p>Acirassi</p>
              <span>Lorem ipsum</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>
						</div>
					</div>
				</div>
			</div>
			<div class="col">
				<div class="container1">
					<div class="front" style="background-image: url(https://unsplash.it/508/508/)">
						<div class="inner">
							<p>Sohanidd</p>
              <span>Lorem ipsum</span>
						</div>
					</div>
					<div class="back">
						<div class="inner">
							<p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Alias cum repellat velit quae suscipit c.</p>

						</div>
					</div>
				</div>
			</div-->
		</div>
 </div>



         <section id="footer-menu" class="animated sections footer-menu">
        <center>
            <div class="container">
                <div class="row">
                    <div class="footer-menu-wrapper">
                        <div class="col-md-12 col-sm-19 col-xs-20">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Quick Links</h5>
                                    <ul>
                                        <li>Virtual Library</li>
                                        <li>OpenSource Technology </li>
                                        <li>WLUG Members</li>
                                        <li>About Us</li>
                                        <li>Recent Updates</li>
                                    </ul>
                                </div>
                            </div>
                           <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Legal</h5>
                                    <ul>
                                        <li>PRIVACY & COOKIES</li>
                                        <li>TERMS OF USE</li>
                                        <li>TRADEMARKS</li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <div class="menu-item">
                                    <h5>Information</h5>
                                    <ul>
                                        <li>SUPPORT</li>
                                        <li>DEVELOPERS</li>
                                        <li>BLOG</li>
                                        <li>Alumni Portal</li>
                                        <li>OpenSource Technology</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </center>
    </section>


    <!--Footer-->
    <footer id="footer" class="footer">
        <div class="container">
            <div class="row">
                <div class="footer-wrapper">
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="footer-brand">
                            <img src="assets/images/wluglogo.png" alt="logo" height="70" width="60"  />
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="copyright">
                            <p> <i class="fa fa-copyright"></i> by <a target="_blank" href="http://wcewlug.org"> WLUG </a>2017. All rights reserved.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>


<div class="scrollup">
            <a href="#"><i class="fa fa-chevron-up"></i></a>
        </div>


        <script src="assets/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="assets/js/vendor/bootstrap.min.js"></script>

        <script src="assets/js/plugins.js"></script>
        <script src="assets/js/modernizr.js"></script>
        <script src="assets/js/main.js"></script>
</body>
</html>